using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPMaterialInstance : MonoBehaviour
{
    [SerializeField]
    private GameObject _creatureHP;
    [SerializeField]
    private CharacterController _characterController;

    private Material _hpMaterial;

    public static HPMaterialInstance _instance;
    private void Start()
    {
        _hpMaterial = _creatureHP.GetComponent<Material>();
        _instance = this;
    }

    private void Update()
    {
        Vector3 raycastStart = _characterController.transform.position + Vector3.up * _characterController.skinWidth;
        Vector3 raycastDirection = Vector3.down;
        RaycastHit hit;
        if (Physics.Raycast(raycastStart, raycastDirection, out hit))
        {
            // 레이캐스트로 부터 얻은 경사면의 법선 벡터
            Vector3 slopeNormal = hit.normal;
            
            // 경사면의 방향 벡터 추출
            Quaternion targetRotation = Quaternion.FromToRotation(Vector3.up, slopeNormal);

            transform.rotation = targetRotation;
        }
    }
    private float _degree = 360;
    public void ChangeHP(float curHP, float FullHP)
    {
        float changedHP = ((_degree * curHP) / FullHP); //a:b = c:d -> a*d = b*c
        _hpMaterial.SetFloat("HP",changedHP);
    }
}
