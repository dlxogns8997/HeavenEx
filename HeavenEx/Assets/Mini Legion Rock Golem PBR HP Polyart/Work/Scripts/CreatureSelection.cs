using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Diagnostics;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.TextCore.Text;
using UnityEngine.UI;

public class CreatureSelection : MonoBehaviour
{
    public List<GameObject> _selectedCharacters = new List<GameObject>();
    private Vector3 _dragStartPosition;
    private bool _isDragging = false;
    private GameObject[] _creatureHPs;

    [SerializeField]
    private Camera _subCamera;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && _isDragging == false)
        {
            _dragStartPosition = Input.mousePosition;
            _dragStartPosition.z = _subCamera.farClipPlane;
            if(_selectedCharacters.Count > 0)
            {
                foreach (GameObject character in _selectedCharacters)
                {
                    character.gameObject.GetComponent<CreatureFSM>()._isSelected = false;

                }
                ClearSelectedCreatures();

            }
            if (_creatureHPs != null)
            {
                SetActiveCreatureHPs(false, _creatureHPs);
                System.Array.Clear(_creatureHPs, 0, _creatureHPs.Length);
            }
            
            _isDragging = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            DragSelection();
            HandleSelection();
            _dragStartPosition = Vector3.zero;
            _isDragging = false;
            _creatureHPs = _selectedCharacters.Select(character => character.transform.Find("CreatureHP").gameObject).ToArray();
            SetActiveCreatureHPs(true ,_creatureHPs);
            CreatureController._isSelect = true;
        }
    }

    private void ClearSelectedCreatures()
    {
        _selectedCharacters.Clear();
    }

    private void SetActiveCreatureHPs(bool active, GameObject[] creatureHPs)
    {
        foreach (GameObject creatureHP in creatureHPs)
        {
            creatureHP.SetActive(active);
        }
    }

    private void OnGUI()
    {
        if (_isDragging)
        {
            var rect = Utils.GetScreenRect(_dragStartPosition, Input.mousePosition);
            Utils.DrawScreenRect(rect, new Color(0f, 0.7f, 0.3f, 0.1f));
            Utils.DrawScreenRectBorder(rect,2, new Color(0f, 0.7f, 0.3f));
        }
    }
    private void OnDrawGizmos()
    {

        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(dragBounds.center, dragBounds.size);

    }
    Bounds dragBounds;
    private void DragSelection()
    {
        GameObject[] characters = FindObjectsOfType<GameObject>().Where(character => character.CompareTag("Creature")).ToArray();

        Vector3 currentMousePosition = Input.mousePosition;
        currentMousePosition.z = _subCamera.farClipPlane;
        Ray startRay = _subCamera.ScreenPointToRay(_dragStartPosition);
        Ray endRay = _subCamera.ScreenPointToRay(currentMousePosition);

        RaycastHit hitStart, hitEnd;
        if (Physics.Raycast(startRay, out hitStart, currentMousePosition.z) && Physics.Raycast(endRay, out hitEnd, currentMousePosition.z))
        {
            Vector3 startPoint = hitStart.point;
            Vector3 endPoint = hitEnd.point;
            dragBounds.SetMinMax(Vector3.Min(startPoint, endPoint), Vector3.Max(startPoint, endPoint));
            Vector3 currentSize = dragBounds.size;
            Vector3 currentPos = dragBounds.center;
            currentSize.y += 10f;
            currentPos.y += 5f;
            dragBounds.size = currentSize;
            dragBounds.center = currentPos;
            foreach (GameObject character in characters)
            {
                if (dragBounds.Contains(character.transform.position))
                {
                    AddToSelectedCharacters(character);
                }
            }
        }
    }

    private void AddToSelectedCharacters(GameObject selectedCharacter)
    {
        CreatureController._isMoving = false; //CreatureController�� animator�� ��ġ�� ������ _isMoving�� false�� �д�.
        selectedCharacter.gameObject.GetComponent<CreatureFSM>()._isSelected = true;
        _selectedCharacters.Add(selectedCharacter);
    }
    private void HandleSelection()
    {
        Debug.Log("Selected " + _selectedCharacters.Count + " characters.");
    }
}
