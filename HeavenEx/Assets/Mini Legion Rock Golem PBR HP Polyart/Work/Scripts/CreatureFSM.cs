using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class CreatureFSM : MonoBehaviour
{
    public enum creatureState
    {
        Idle,
        Trace,
        Attack,
        BeAttacked,
        Die,
    }
    [SerializeField]
    private CharacterController _characterController;
    [SerializeField]
    private NavMeshAgent _navMeshAgent;
    [SerializeField]
    private Animator _animator;
    private creatureState _creatureState = creatureState.Idle;

    [SerializeField]
    private float _traceDistance;
    private Vector3 _enemyPos;
    private void Idle()
    {
        try
        {
            _animator.SetBool("IsWalk", false);
            RaycastHit[] enemys = Physics.SphereCastAll(transform.position, 12f, Vector3.up);
            if (enemys.Length > 0)
            {
                Dictionary<Vector3, float> dicDistances = new();
                foreach (RaycastHit enemy in enemys)
                {
                    if (enemy.transform.gameObject.layer == LayerMask.NameToLayer("Enemy"))
                    {
                        dicDistances.Add(enemy.transform.position, Vector3.Distance(transform.position, enemy.transform.position));

                    }
                }
                float distance = dicDistances.Values.Min();
                foreach (RaycastHit enemy in enemys)
                {
                    if (dicDistances[enemy.transform.position] == distance && distance > _traceDistance)
                    {
                        _enemyPos = enemy.transform.position;
                        _creatureState = creatureState.Trace;
                        return;
                    }
                }
            }
        }
        catch (Exception)
        {
            return;
        }

    }
    public float animatorSpeedMultiplier = 0.2f;
    private void Trace()
    {
       // CreatureController._isMoving = false; //CreatureController�� animator�� ��ġ�� ������ _isMoving�� false�� �д�.
        Vector3 targetPosition = new Vector3(_enemyPos.x, _characterController.transform.position.y, _enemyPos.z);
        Vector3 moveDirection = targetPosition - _characterController.transform.position;
        float distanceToTarget = moveDirection.sqrMagnitude;
        if (distanceToTarget > (_traceDistance * _traceDistance) )
        {
            _navMeshAgent.destination = targetPosition;
            float currentWalkSpeed = _navMeshAgent.desiredVelocity.magnitude;
            _animator.SetFloat("WalkSpeed", currentWalkSpeed * animatorSpeedMultiplier);
            _animator.SetBool("IsWalk", true);

            if (_navMeshAgent.desiredVelocity.normalized != Vector3.zero)
            {
                Quaternion newRotation = Quaternion.LookRotation(_navMeshAgent.desiredVelocity.normalized);
                _navMeshAgent.transform.rotation = Quaternion.Slerp(_navMeshAgent.transform.rotation, newRotation, Time.deltaTime * 10f);
            }
        }
        else
        {
            _creatureState = creatureState.Idle;
        }
    }
    public bool _isSelected = false;
    void Update()
    {
        if (_isSelected == false)
        {
            switch (_creatureState)
            {
                case creatureState.Idle:
                    _navMeshAgent.stoppingDistance = 4f;
                    Idle();
                    break;
                case creatureState.Trace:
                    _navMeshAgent.stoppingDistance = 2f;
                    Trace();
                    break;
            }
        }
        else
        {
            _creatureState = creatureState.Idle;
        }
    }
}
