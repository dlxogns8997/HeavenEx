using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.TextCore.Text;

public class CreatureController : MonoBehaviour
{
    private List<List<NavMeshAgent>> _navMeshAgentList = new();
    private List<List<Animator>> _animatorList = new();
    [SerializeField]
    private Camera _subCamera;
    [SerializeField]
    private CreatureSelection _creatureSelection;
    void Start()
    {
    }
    private Vector3 _targetPosition;
    public static bool _isMoving = false;
    public static bool _isSelect = false;
    void Update()
    {
        AutoSelectCreature();
        SetTarget();
        OldCreatureMoveStop();
        if (_isMoving)
        {
            SelectedCreatureLookAt();
            SelectedCreatureMoveTo();
        }
    }
    
    private void AutoSelectCreature()
    {
        if (_isSelect && _creatureSelection._selectedCharacters.Count > 0)
        {
            if (_navMeshAgentList.Count <= 0 && _animatorList.Count <= 0)
            {
                _navMeshAgentList.Add(_creatureSelection._selectedCharacters.Select(character => character.GetComponent<NavMeshAgent>()).ToList());
                _animatorList.Add(_creatureSelection._selectedCharacters.Select(character => character.GetComponent<Animator>()).ToList());
            }
            else if (_navMeshAgentList.Count >= 1 && _animatorList.Count >= 1)
            {
                List<NavMeshAgent> curNavMeshAgentGroup = _creatureSelection._selectedCharacters.Select(characterGroup => characterGroup.GetComponent<NavMeshAgent>()).ToList();
                List<Animator> curAnimatorGroup = _creatureSelection._selectedCharacters.Select(character => character.GetComponent<Animator>()).ToList();
                if (_isMoving)
                {
                    _navMeshAgentList.Add(_navMeshAgentList[0].Where(navMeshAgent => !curNavMeshAgentGroup.Contains(navMeshAgent)).ToList());
                    _animatorList.Add(_animatorList[0].Where(animator => !curAnimatorGroup.Contains(animator)).ToList());
                    _isMoving = false;
                }
                _navMeshAgentList[0] = curNavMeshAgentGroup;
                _animatorList[0] = curAnimatorGroup;
                SelectedCreatureMoveStop();
                
            }
            
            _isSelect = false;
        }
    }

   

    private bool _isStop = false;
    private void OldCreatureMoveStop()
    {
        if (_navMeshAgentList.Count > 1 && _animatorList.Count > 1)
        {
            foreach ((List<NavMeshAgent> navMeshAgentGroup, List<Animator> animatorGroup) in _navMeshAgentList.Zip(_animatorList, (agent, anim) => (agent, anim)))
            {
                if(navMeshAgentGroup != _navMeshAgentList[0] && animatorGroup != _animatorList[0])
                {
                    foreach ((NavMeshAgent navMeshAgent2, Animator animator2) in navMeshAgentGroup.Zip(animatorGroup, (agent, anim) => (agent, anim)))
                    {
                        if (animator2.GetCurrentAnimatorStateInfo(0).IsName("Walk") && navMeshAgent2.desiredVelocity == Vector3.zero && _isStop == false)
                        {
                            RemoveOldCreature(navMeshAgentGroup, animatorGroup);
                            return;
                        }
                    }
                }
            }
        }
    }

    private void RemoveOldCreature(List<NavMeshAgent> navMeshAgentGroupToDel, List<Animator> animatorGroupToDel)
    {
        foreach ((NavMeshAgent navMeshAgent, Animator animator) in navMeshAgentGroupToDel.Zip(animatorGroupToDel, (agent, anim) => (agent, anim)))
        {
            navMeshAgent.ResetPath();
            animator.SetBool("IsWalk", false);
        }
        _navMeshAgentList.Remove(navMeshAgentGroupToDel);
        _animatorList.Remove(animatorGroupToDel);
    }

    private void SetTarget()
    {
        if (Input.GetMouseButtonDown(1) && _creatureSelection._selectedCharacters.Count > 0)
        {
            Vector3 mousePosition = Input.mousePosition;
            mousePosition.z = _subCamera.farClipPlane;
            Ray ray = _subCamera.ScreenPointToRay(mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000))
            {
                foreach (NavMeshAgent navMeshAgent in _navMeshAgentList[0])
                {
                    _targetPosition = new Vector3(hit.point.x, navMeshAgent.transform.position.y, hit.point.z);
                }
                _isMoving = true;
            }
        }
    }

    public float animatorSpeedMultiplier = 0.2f;
    private void SelectedCreatureMoveTo()
    {

        foreach ((NavMeshAgent navMeshAgent, Animator animator) in _navMeshAgentList[0].Zip(_animatorList[0], (agent, anim) => (agent, anim)))
        {
            Vector3 moveDirection = _targetPosition - navMeshAgent.transform.position;
            float distanceToTarget = moveDirection.sqrMagnitude;
            if (distanceToTarget >= navMeshAgent.stoppingDistance)
            {
                navMeshAgent.destination = _targetPosition;
                float currentWalkSpeed = navMeshAgent.desiredVelocity.magnitude;
                animator.SetFloat("WalkSpeed", currentWalkSpeed * animatorSpeedMultiplier);
                animator.SetBool("IsWalk", true);
                if(navMeshAgent.gameObject.GetComponent<CreatureFSM>()._isSelected == false) 
                {
                    _navMeshAgentList[0].Remove(navMeshAgent);
                    _animatorList[0].Remove(animator);
                    return;
                }
            }
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Walk") && navMeshAgent.desiredVelocity == Vector3.zero)
            {
                SelectedCreatureMoveStop();
                _isMoving = false;
                return;
            }

        }
    }

    private void SelectedCreatureMoveStop()
    {
        foreach ((NavMeshAgent navMeshAgent, Animator animator) in _navMeshAgentList[0].Zip(_animatorList[0], (agent, anim) => (agent, anim)))
        {
            navMeshAgent.ResetPath();
            animator.SetBool("IsWalk", false);
        }

    }

    private float _rotateSpeed = 10f;
    private void SelectedCreatureLookAt()
    {
        foreach (NavMeshAgent navMeshAgent in _navMeshAgentList[0])
        {
            if (navMeshAgent.desiredVelocity.normalized != Vector3.zero)
            {
                Quaternion newRotation = Quaternion.LookRotation(navMeshAgent.desiredVelocity.normalized);
                navMeshAgent.transform.rotation = Quaternion.Slerp(navMeshAgent.transform.rotation, newRotation, Time.deltaTime * _rotateSpeed);
            }
        }
    }
}
